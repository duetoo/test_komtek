from django.db import models


class ReferenceBook(models.Model):
    internal_id = models.PositiveIntegerField()
    name = models.TextField(max_length=250,
                            verbose_name='Наименование')
    short_name = models.CharField(max_length=40,
                                  verbose_name='Короткое наименование')
    description = models.TextField(max_length=300,
                                   verbose_name='Описание')
    version = models.CharField(max_length=50,
                               verbose_name='Версия')
    come_into_force = models.DateField(verbose_name='Дата ввода справочника')

    class Meta:
        unique_together = ('name', 'version',)
        verbose_name = 'Справочник'
        verbose_name_plural = 'Список справочников'

    def __str__(self):
        return f'{self.name} - {self.come_into_force} - {self.version}'


class Detail(models.Model):
    book = models.ForeignKey(ReferenceBook,
                             on_delete=models.CASCADE,
                             related_name='details',
                             verbose_name='Наименование справочника'
                             )
    code = models.CharField(max_length=50,
                            verbose_name='Код элемента')
    value = models.CharField(max_length=50,
                             verbose_name='Значение элемента')

    class Meta:
        verbose_name = 'Элемент справочника'
        verbose_name_plural = 'Список элементов справочника'

    def __str__(self):
        return f'{self.book} - {self.value}'
