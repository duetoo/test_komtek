from django.utils.dateparse import parse_date
from django.http import Http404
from rest_framework import generics, views
from rest_framework.response import Response
from .models import ReferenceBook
from .serialisers import ReferenceBookSerializer, DetailSerializer


class ReferenceBookView(generics.ListAPIView):
    serializer_class = ReferenceBookSerializer
    queryset = ReferenceBook.objects.all()

    def get_view_name(self):
        return f'Список справочников'


class ReferenceBookActualForDateView(generics.ListAPIView):
    serializer_class = ReferenceBookSerializer

    def get_queryset(self):
        actual_reference = ReferenceBook.objects.filter(come_into_force__lte=
                                                        parse_date(self.request.query_params['date'])).\
            order_by('internal_id', '-come_into_force', '-version').\
            distinct('internal_id')
        return actual_reference

    def get_view_name(self):
        return f'Список актуальных справочников на указанную дату'


class DetailActualVersionView(views.APIView):
    def get_object(self, pk):
        try:
            return ReferenceBook.objects.filter(internal_id=pk).latest('version')
        except ReferenceBook.DoesNotExist:
            raise Http404

    def get(self, request, pk=None):
        book_object = self.get_object(pk)
        detail = book_object.details
        serializer = DetailSerializer(detail, many=True)
        return Response(serializer.data)

    def get_view_name(self):
        return f'Элементы заданного справочника текущей версии'


class DetailByVersionView(views.APIView):
    def get_object(self, pk):
        try:
            return ReferenceBook.objects.get(internal_id=pk,
                                             version=self.request.query_params['version'])
        except ReferenceBook.DoesNotExist:
            raise Http404

    def get(self, request, pk=None):
        book_object = self.get_object(pk)
        detail = book_object.details
        serializer = DetailSerializer(detail, many=True)
        return Response(serializer.data)

    def get_view_name(self):
        return f'Элементы заданного справочника текущей версии'
