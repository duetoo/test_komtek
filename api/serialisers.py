from rest_framework import serializers
from .models import ReferenceBook, Detail


class ReferenceBookSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReferenceBook
        fields = '__all__'


class DetailSerializer(serializers.ModelSerializer):

    book = serializers.StringRelatedField()

    class Meta:
        model = Detail
        fields = '__all__'
