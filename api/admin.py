from django.contrib import admin
from .models import ReferenceBook, Detail


@admin.register(ReferenceBook)
class ReferenceBoolAdmin(admin.ModelAdmin):
    list_display = ('id',
                    'internal_id',
                    'name',
                    'short_name',
                    'version',
                    'description',
                    'come_into_force',)
    ordering = ('id',)
    list_filter = ('name', 'come_into_force',)


@admin.register(Detail)
class DetailAdmin(admin.ModelAdmin):
    list_display = ('book', 'code', 'value',)
