from django.urls import path
from rest_framework_swagger.views import get_swagger_view
from .views import (ReferenceBookView,
                    ReferenceBookActualForDateView,
                    DetailActualVersionView,
                    DetailByVersionView
                    )

schema_view = get_swagger_view(title='Medical Reference Book API')

urlpatterns = [
    path('books/', ReferenceBookView.as_view()),
    path('books/actual/', ReferenceBookActualForDateView.as_view()),
    path('books/actual_version/<int:pk>/elements/', DetailActualVersionView.as_view()),
    path('books/<int:pk>/elements/', DetailByVersionView.as_view()),
    path('openapi/', schema_view),
]
